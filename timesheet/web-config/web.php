<?php
/**
* Global Customization for our application
*/
$webLocal = [
/*##########################
 # 2 - Custom components #
##########################*/
  'defaultRoute' => 'timesheet/dashboard/index',
  'runtimePath' => '/tmp',
  'modules' => [
    'timesheet' => 'nc\timesheet\TimesheetModule',
  ] ,
];

/*#########################################
 # 4 - Developement Specific Config       #
 ##########################################*/
if (YII_DEBUG) {

}

return $webLocal;
?>
