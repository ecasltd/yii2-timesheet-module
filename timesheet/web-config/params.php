<?php

$localParams = [
  /** End of System Email Parameters **/
	'timesheet-status' => [
          0 => 'Not yet verified',
          1 => 'Submited',
          2 => 'Approved',
          -1 => 'Rejected',
          -2 => 'Returned for Correction',
  ],
  /** Transition state for Leave Request **/
  'leave-req-state' => [
          0 => 'Not yet verified',
          1 => 'Submited',
          2 => 'Approved',
          -1 => 'Rejected',
  ],
];
return $localParams;
