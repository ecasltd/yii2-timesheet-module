<?php
/**
* Web Application Customization for our application
*/
$commonLocal = [
  'id' => 'timesheet',
  'name' => 'Timesheet',
  'components' => [
    'db' => [
      'class' => 'yii\db\Connection',
      'dsn' => 'mysql:host=db;dbname=timesheet',
      'username' => 'root',
      'password' => '',
      'charset' => 'utf8'
    ],
  ]
/*##########################
 # 2 - Custom components #
##########################*/
];

/*#########################################
 # 4 - Developement Specific Config       #
 ##########################################*/
if (YII_DEBUG) {

}

return $commonLocal;
?>
