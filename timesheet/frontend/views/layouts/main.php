<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Dashboard', 'url' => ['/timesheet/dashboard']],
            ['label' => 'Timesheet', 'url' => '#', 'items' => [
              ['label' => \Yii::t('nc', 'Dashboard'), 'url' => ['/timesheet/dashboard/index'], 'visible' => \Yii::$app->user->can('/timesheet/dashboard/*')],
              ['label' => \Yii::t('nc', 'Managers'), 'url' => ['/timesheet/manager/index'], 'visible' => \Yii::$app->user->can('/timesheet/manager/*')],
              ['label' => \Yii::t('nc', 'Time Sheet'), 'url' => ['/timesheet/timesheet/index'], 'visible' => \Yii::$app->user->can('/timesheet/timesheet/*')],
              ['label' => \Yii::t('nc', 'Meeting'), 'url' => ['/timesheet/meeting/index'], 'visible' => \Yii::$app->user->can('/timesheet/meeting/*')],
              ['label' => \Yii::t('nc', 'Leave Requests'), 'url' => ['/timesheet/leave-request/index'], 'visible' => \Yii::$app->user->can('/timesheet/leave-request/*')],
              ['label' => \Yii::t('nc', 'Job Codes'), 'url' => ['/timesheet/job-code/index'], 'visible' => \Yii::$app->user->can('/timesheet/job-code/*')],
              ['label' => \Yii::t('nc', 'Files'), 'url' => ['/timesheet/file/index'], 'visible' => \Yii::$app->user->can('/timesheet/file/*')],
            ]],
            ['label' => 'User Admin', 'url' => ['/user/admin']],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/user/security/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/user/security/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Evolve Cosmos Allied Services <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
