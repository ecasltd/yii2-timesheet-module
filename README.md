# Timesheet Module

This Yii2 module provide a clone of the famous "myworkday" application.

## URL:

__Timesheet__

* User submit timesheet: /timesheet/dashboard/timesheet
* Manager approve timesheet: /timesheet/manager/timesheet
* Administration approve timesheet: /timesheet/admin/timesheet


## Deployment Information

```bash
# Perform Database Migration
./yii migrate --migrationPath=@yii/rbac/migrations
./yii migrate --migrationPath=@dektrium/user/migrations
./yii migrate --migrationPath=@nc/timesheet/migrations
# User with name `administrator` must be defined as AdminUser via userModule
./yii user/create administrator@email.com administrator adminPass
./yii user/confirm administrator
# Create some dummy manager
for i in {0..9}; do ./yii user/create manager${i}@example.com manager$i managerPassword; done;
for i in {0..9}; do ./yii user/confirm manager$i; done;
# Create some dummy User
for i in {0..9}; do ./yii user/create user${i}@example.com user$i userPassword; done;
for i in {0..9}; do ./yii user/confirm user$i; done;
```
