<?php
namespace nc\timesheet;

use yii\console\Application as ConsoleApp;
use Yii;
class TimesheetModule extends \yii\base\Module {
	public $controllerNamespace = 'nc\timesheet\controllers';
	public $defaultRoute = 'dashboard';
	function init(){
		if (Yii::$app instanceof ConsoleApp){
			$this->controllerNamespace = 'nc\timesheet\commands';
		}
		Yii::$app->i18n->translations['nc*'] = [
		    'class' => 'yii\i18n\PhpMessageSource',
		    'basePath' => '@nc/timesheet/messages',
		];
	}
}
