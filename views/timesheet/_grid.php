<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
?>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            'owner.username:ntext:Owner',
            'approvedBy.username:ntext:Approved By',
            'approved_at',
            // 'month',
            // 'year',
            // 'week',
            // 'state',
            // 'comment:ntext',
            [ 'attribute' => 'state',
              'value' => function($model){
                return $model::state($model['state']);
              }, 'filter' => Yii::$app->params['timesheet-status']
            ],

            [
              'class' => 'yii\grid\ActionColumn',
              'template' => '{view} {update} {approve} {reject} {delete}',
              'buttons' => [
                'approve' => function ($url, $model, $key) {
                  return Html::a('<span class="glyphicon glyphicon-check"></span>', ['approve', 'id' => $model->id, 'state' => 2], [
                      'title' => Yii::t('nc', 'Approve'),
                      'aria-label' => Yii::t('nc', 'Approve'),
                      'data-confirm' => Yii::t('nc', 'Are you sure you want to approve this timesheet?'),
                      'data-method' => 'post',
                      'data-pjax' => '0',
                  ]);
                },
                'reject' => function ($url, $model, $key) {
                  return Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', ['approve', 'id' => $model->id, 'state' => 2], [
                      'title' => Yii::t('nc', 'Reject'),
                      'aria-label' => Yii::t('nc', 'Reject'),
                      'data-confirm' => Yii::t('nc', 'Are you sure you want to reject this timesheet?'),
                      'data-method' => 'post',
                      'data-pjax' => '0',
                  ]);
                }
              ]],
        ],
    ]); ?>
<?php Pjax::end(); ?>
