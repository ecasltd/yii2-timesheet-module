<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\Timesheet */

$this->title = Yii::t('nc', 'Enter Timesheet for {title}', ['title' => $sheet->getTitle()]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('nc', 'Timesheets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="timesheet-create">

    <h1><?= Html::encode($this->title) ?></h1>



</div>
