<?php

use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use nc\timesheet\models\TimeEntry;


$events = ArrayHelper::index($model->entries, 'id', 'date');
$summary = []; $total = 0;
foreach ($model->entries as $e) {
  if (!array_key_exists($key = $e->jobCode->name, $summary)) $summary[$key] = 0;
  $summary[$key] += $e->duration; $total += $e->duration;
}
/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\Timesheet */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('nc', 'Timesheet'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= Html::a(Yii::t('nc', '{icon} Export PDF',  ['icon' => Html::icon('download')]), ['export-pdf', 'id' => $model->id], ['class' => 'btn btn-primary pull-right']) ?>

<h1><small><?= Yii::t('nc', 'Timesheet') ?></small> <?= Html::encode($this->title) ?> <span class="badge"><?= $model::state($model->state); ?></span></h1>

<div class="timesheet-view">
  <div class="table-responsive">
  <table class="table table-bordered">
    <thead>
      <tr>
      <?php $date = $model->dateFrom;
      $end_date = $model->dateTo;
      $dates = [];
      while ($date <= $end_date){ ?>
        <th><?= date('D d/m', strtotime($date)); ?></th>
      <?php
        $dates[] = $date;
        $date = date('Y-m-d', strtotime("$date + 1 day"));
      }  ?>
      </tr>
    </thead>
    <tbody>
      <tr>
        <?php foreach ($dates as $date): ?>
          <td class="event-panel"><div style="min-height: 200px;">
            <?php foreach (ArrayHelper::getValue($events, $date, []) as $id => $e): ?>
              <?= Html::tag('h5', Yii::t('nc', '{icon} {duration} hr: {jobcode}', [
                'icon' => Html::icon('time'),
                'duration' => $e['duration'],
                'jobcode' => $e->jobCode->name,
                ]),[
                  'style' => ['color' => $e->jobCode->color],
                'title' => $e['description'],
                'class' => 'btn btn-default btn-block event-btn',
                'data-date' => $date,
                'data-id' => $id,
                'data-duration' => $e['duration'],
                'data-description' => $e['description'],
                'data-jobcode_id' => $e['jobcode_id'],
              ]); ?>
            <?php endforeach; ?>
          </div></td>
        <?php endforeach; ?>
      </tr>
    </tbody>
  </table>

</div>
</div>
  <h3><?= Yii::t('nc', 'Time Entries'); ?></h3>
  <?= GridView::widget([
    'dataProvider' => new ArrayDataProvider(['allModels' => $model->entries]),
    'columns' => [
      'date',
      ['attribute' => 'date', 'value' => function($d){ return date('l', strtotime($d->date)); }, 'format' => 'ntext', 'label' => 'Date of Week'],
      'jobCode.name:ntext:Job Code',
      'description:ntext',
      'duration:integer',
    ]

  ]); ?>

  <h3><?= Yii::t('nc', 'Employee'); ?></h3>
  <div class="row"><div class="col-md-6">
  <?= DetailView::widget([
    'model' => $model->owner,
    'attributes' => [
      'profile.name',
      'email',
    ]
  ]); ?>
  </div><div class="col-md-6">

    <p class="text"><?= Yii::t('nc', '<strong>{user}</strong> comment on submission:', ['user' => $model->owner->username]); ?></p>
    <blockquote><?= $model->comment; ?></blockquote>
  </div></div>
<div class="row">
  <div class="col-md-6">
    <h4 class="text-info"><?= Yii::t('nc', 'Total time spent by Job Code'); ?></h4>
    <ul class="summary">
    <?php foreach($summary as $label => $value): ?>
      <li><strong><?= $label ?>:</strong> <span class="pull-right"><?= $value ?> hr</span></li>
    <?php endforeach; ?>
    </ul>
    <hr>
    <ul class=""><li>
    <strong><?= Yii::t('nc', 'Total') ?>:</strong> <span class="pull-right"><?= $total ?> hr</span></li></ul>
  </div>
  <?php if ($model->state != 0): // Skip if Draft ?>
  <div class="col-md-6">
    <div class="panel panel-primary">
      <div class="panel-heading"><?= Yii::t('nc', 'Management Panel') ?></div>
      <div class="panel-body">
      <?php $form = ActiveForm::begin(['id' => 'approval-form']); ?>

      <?= $form->field($model, 'approval_note')->textarea(['rows' => 4]) ?>

      <?= Html::submitButton(Yii::t('nc', '{icon} Approve', ['icon' => Html::icon('send')]), ['class' => 'btn btn-success', 'name' => $model->formName() . "[state]", 'value' => 2 ]) ?>

      <?= Html::submitButton(Yii::t('nc', '{icon} Need Correction', ['icon' => Html::icon('save')]), ['class' => 'btn btn-danger', 'name' => $model->formName() . "[state]", 'value' => -2 ]) ?>

      <?= Html::submitButton(Yii::t('nc', '{icon} Reject', ['icon' => Html::icon('save')]), ['class' => 'btn btn-warning', 'name' => $model->formName() . "[state]", 'value' => -1 ]) ?>

      <?php ActiveForm::end(); ?>
  </div></div></div>
  <?php endif; ?>
</div>
