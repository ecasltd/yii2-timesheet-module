<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('nc', 'Files');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-index">
  <p class="pull-right">
      <?= Html::a(Yii::t('nc', 'New File'), ['create'], ['class' => 'btn btn-success']) ?>
      <?= Html::a(Yii::t('nc', 'Batch Upload'), ['batch-upload'], ['class' => 'btn btn-primary']) ?>
  </p>
    <h1><?= Html::encode($this->title) ?></h1>


<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // 'filename',

            ['attribute' => 'url', 'value' => function($model){
              // if (strpos($model->type, 'image')!== false){
              //   $text = Html::img([$model->url], ['class' => 'img-responsive', 'style' => ['max-height' => 100]]);
              // } else {
              //   $text = $model->filename;
              // }
              $text = $model->filename;
              return Html::a($text, $model->url, [
                  'title' => Yii::t('nc', 'Download'),
                  'target' => '_blank',
                  'class' => 'linksWithTarget',
                  'data-pjax' => '0',
              ]);
            }, 'format' => 'raw', 'label' => Yii::t('nc', 'Download')],
            'description:ntext',
            'createdBy.username:ntext:'.Yii::t('nc', 'Created By'),
            'type',
            // 'size',
            // 'description:ntext',
            // 'thumbs:ntext',
            // 'created_at',
            'updated_at:datetime',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
