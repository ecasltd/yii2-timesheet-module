<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\JobCode */

$this->title = Yii::t('nc', 'Update {modelClass}: ', [
    'modelClass' => 'JobCode',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('nc', 'JobCodes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('nc', 'Update');
?>
<div class="jobcode-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
