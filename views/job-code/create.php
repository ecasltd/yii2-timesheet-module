<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\JobCode */

$this->title = Yii::t('nc', 'Create JobCode');
$this->params['breadcrumbs'][] = ['label' => Yii::t('nc', 'JobCodes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jobcode-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
