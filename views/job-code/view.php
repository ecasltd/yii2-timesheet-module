<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\JobCode */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('nc', 'JobCodes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jobcode-view">
  <p class="pull-right">
      <?= Html::a(Yii::t('nc', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?= Html::a(Yii::t('nc', 'Delete'), ['delete', 'id' => $model->id], [
          'class' => 'btn btn-danger',
          'data' => [
              'confirm' => Yii::t('nc', 'Are you sure you want to delete this item?'),
              'method' => 'post',
          ],
      ]) ?>
  </p>
    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            'name',
            'description:ntext',
            'color',
        ],
    ]) ?>

</div>
