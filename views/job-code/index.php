<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel nc\timesheet\models\JobCodeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('nc', 'JobCodes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jobcode-index">
  <p class="pull-right">
      <?= Html::a(Yii::t('nc', 'Create JobCode'), ['create'], ['class' => 'btn btn-success']) ?>
  </p>

    <h1><?= Html::encode($this->title) ?></h1>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'description:ntext',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
