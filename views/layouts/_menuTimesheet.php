<?php return [
  ['label' => \Yii::t('nc', 'Dashboard'), 'url' => ['/timesheet/dashboard/index'], 'visible' => \Yii::$app->user->can('/timesheet/dashboard/index')],

  ['label' => \Yii::t('nc', 'Managers'), 'url' => ['/timesheet/manager/index'], 'visible' => \Yii::$app->user->can('/timesheet/manager/index')],

  ['label' => \Yii::t('nc', 'Time Sheet'), 'url' => ['/timesheet/timesheet/index'], 'visible' => \Yii::$app->user->can('/timesheet/timesheet/index')],

  ['label' => \Yii::t('nc', 'Meeting'), 'url' => ['/timesheet/meeting/index'], 'visible' => \Yii::$app->user->can('/timesheet/meeting/index')],

  ['label' => \Yii::t('nc', 'Leave Requests'), 'url' => ['/timesheet/leave-request/index'], 'visible' => \Yii::$app->user->can('/timesheet/leave-request/index')],

  ['label' => \Yii::t('nc', 'Job Codes'), 'url' => ['/timesheet/job-code/index'], 'visible' => \Yii::$app->user->can('/timesheet/job-code/index')],

  ['label' => \Yii::t('nc', 'Files'), 'url' => ['/timesheet/file/index'], 'visible' => \Yii::$app->user->can('/timesheet/file/index')],
];
