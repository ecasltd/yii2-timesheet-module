<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\Meeting */

$this->title = Yii::t('nc', 'Create Meeting');
$this->params['breadcrumbs'][] = ['label' => Yii::t('nc', 'Meetings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meeting-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
