<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\LeaveRequest */

$this->title = Yii::t('nc', 'Create Leave Request');
$this->params['breadcrumbs'][] = ['label' => Yii::t('nc', 'Leave Requests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="leave-request-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
