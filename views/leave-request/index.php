<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel nc\timesheet\models\LeaveRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('nc', 'Leave Requests');
$this->params['breadcrumbs'][] = $this->title;

// Support change approval state in batch mode
$this->registerJs("
    $(document).ready(function(){
      $('.batchAction').click(function(e){
        e.preventDefault();
        var keys = $('#timesheet-list').yiiGridView('getSelectedRows');
        var csrf = yii.getCsrfToken();
        var btnUrl = $(this).attr('href');
        $.ajax({
            type: 'POST',
            url : btnUrl,
            data : {row_id: keys, _csrf: csrf},
            success : function() {
              location.reload();
            },
            error : function(){
              location.reload();
            }
        });
    });
});", \yii\web\View::POS_READY);
?>
<div class="leave-request-index">

  <p class="pull-right">
    <?= Html::a(Yii::t('nc', 'Approve'), ['batch-approval', 'state' => 2], ['class' => 'btn btn-success batchAction', 'title' => Yii::t('nc', 'Approve Selected Leave Requests')]) ?>
    <?= Html::a(Yii::t('nc', 'Reject'), ['batch-approval', 'state' => -1], ['class' => 'btn btn-danger batchAction', 'title' => Yii::t('nc', 'Reject Selected Leave Requests')]) ?>
  </p>
    <h1><?= Html::encode($this->title) ?></h1>

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
          ['class' => 'yii\grid\SerialColumn'],
          // 'owner.username:ntext:Owner',
          // 'approved_at',
          'start_date',
          'end_date',
          'note:ntext',
          // 'code',
          // 'all_day',
          // 'number',
          // 'note:ntext',
          // 'approvedBy.username:ntext:Approved By',
          // 'approval_note:ntext',
          // 'approved_at',

          [ 'attribute' => 'state',
            'value' => function($model){
              return $model::state($model['state']);
            }, 'filter' => Yii::$app->params['leave-req-state']
          ],
          [ 'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {approve} {reject} {delete}',
            'buttons' => [
              'approve' => function ($url, $model, $key) {
                return Html::a('<span class="glyphicon glyphicon-check"></span>', ['approve', 'id' => $model->id, 'state' => 2], [
                    'title' => Yii::t('nc', 'Approve'),
                    'aria-label' => Yii::t('nc', 'Approve'),
                    'data-confirm' => Yii::t('nc', 'Are you sure you want to approve this leave request?'),
                    'data-method' => 'post',
                    'data-pjax' => '0',
                ]);
              },
              'reject' => function ($url, $model, $key) {
                return Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', ['approve', 'id' => $model->id, 'state' => -1], [
                    'title' => Yii::t('nc', 'Reject'),
                    'aria-label' => Yii::t('nc', 'Reject'),
                    'data-confirm' => Yii::t('nc', 'Are you sure you want to reject this leave request?'),
                    'data-method' => 'post',
                    'data-pjax' => '0',
                ]);
              }
            ],
          ],
          ['class' => 'yii\grid\CheckboxColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
