<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
$this->title = "Main Menu";
?>

<div class="panel panel-danger">
  <div class="panel-heading">
    <?= Html::a(Yii::t('nc', '{icon} New Leave Request', ['icon' => Html::icon('asterisk')]), ['leave-request'], ['class' => 'btn btn-danger pull-right']) ?>
    <h4><?= Yii::t('nc', '{icon} My Leave Request', ['icon' => Html::icon('plane')]) ?></h4>
  </div>
  <div class="panel-body">
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                // 'owner.username:ntext:Owner',
                // 'approved_at',
                'start_date',
                'end_date',
                'note:ntext',
                // 'code',
                // 'all_day',
                // 'number',
                // 'note:ntext',
                // 'approvedBy.username:ntext:Approved By',
                // 'approval_note:ntext',
                // 'approved_at',

                [ 'attribute' => 'state',
                  'value' => function($model){
                    return $model::state($model['state']);
                  }, 'filter' => Yii::$app->params['leave-request-status']
                ],
                [ 'class' => 'yii\grid\ActionColumn',
                  'template' => '{view}',
                  'buttons' => [
                    'view' => function ($url, $model, $key) {
                      return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['leave-request', 'id' => $model->id], [
                          'title' => Yii::t('nc', 'View'),
                      ]);
                    }
                  ]],
              ],
        ]); ?>
    <?php Pjax::end(); ?>

  </div>
</div>
