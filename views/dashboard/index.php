<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
$this->title = "Dashboard";
?>

<h1><?= $this->title; ?></h1>

<?= $this->render('_timesheet', ['dataProvider' => $timesheet]); ?>
<div class="row">
  <div class="col-md-6">
    <?= $this->render('_meeting', ['dataProvider' => $meeting]); ?>
  </div>

  <div class="col-md-6">
    <?= $this->render('_leave', ['dataProvider' => $leaveRequest]); ?>
  </div>

  <div class="col-md-12">
    <?= $this->render('_files', ['dataProvider' => $files]); ?>
  </div>
</div>
