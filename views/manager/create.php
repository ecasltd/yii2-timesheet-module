<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\ManagerUser */

$this->title = Yii::t('nc', 'Create Manager User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('nc', 'Manager Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manager-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
