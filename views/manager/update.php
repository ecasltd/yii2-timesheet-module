<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\ManagerUser */

$this->title = Yii::t('nc', 'Update {modelClass}: ', [
    'modelClass' => 'Manager User',
]) . $model->manager_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('nc', 'Manager Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->manager_id, 'url' => ['view', 'manager_id' => $model->manager_id, 'user_id' => $model->user_id]];
$this->params['breadcrumbs'][] = Yii::t('nc', 'Update');
?>
<div class="manager-user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
