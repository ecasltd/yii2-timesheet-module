<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('nc', 'Manager Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manager-user-index">
  <p class="pull-right">
      <?= Html::a(Yii::t('nc', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
      <?= Html::a(Yii::t('nc', 'Assignments'), ['batch'], ['class' => 'btn btn-primary']) ?>
  </p>
    <h1><?= Html::encode($this->title) ?></h1>


<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'manager.username:ntext:Manager',
            'user.username:ntext:Employee',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
