<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\ManagerUser */

$this->title = $model->manager_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('nc', 'Manager Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manager-user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('nc', 'Update'), ['update', 'manager_id' => $model->manager_id, 'user_id' => $model->user_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('nc', 'Delete'), ['delete', 'manager_id' => $model->manager_id, 'user_id' => $model->user_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('nc', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'manager_id',
            'user_id',
        ],
    ]) ?>

</div>
