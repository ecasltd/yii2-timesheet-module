<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\InvitationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="invitation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'meeting_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'approved_at') ?>

    <?= $form->field($model, 'accepted') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('nc', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('nc', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
