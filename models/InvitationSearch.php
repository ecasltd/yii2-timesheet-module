<?php

namespace nc\timesheet\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use nc\timesheet\models\Invitation;

/**
 * InvitationSearch represents the model behind the search form about `nc\timesheet\models\Invitation`.
 */
class InvitationSearch extends Invitation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'meeting_id', 'user_id', 'accepted'], 'integer'],
            [['decided_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Invitation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'meeting_id' => $this->meeting_id,
            'user_id' => $this->user_id,
            'decided_at' => $this->decided_at,
            'accepted' => $this->accepted,
        ]);

        return $dataProvider;
    }
}
