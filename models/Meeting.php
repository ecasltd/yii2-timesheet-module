<?php

namespace nc\timesheet\models;
use yii\helpers\ArrayHelper;
use Yii;
use dektrium\user\models\User;

/**
 * This is the model class for table "meeting".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $note
 * @property string $all_day
 * @property string $start_date
 * @property string $end_date
 */
class Meeting extends \yii\db\ActiveRecord
{
    public $userIDs;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%meeting}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_date', 'end_date', 'userIDs', 'name'], 'required'],
            [['user_id'], 'integer'],
            [['note'], 'string'],
            [['start_date', 'end_date'], 'date', 'format' => 'php:Y-m-d H:i:s'],
            [['end_date'], 'compare', 'operator' => '>=', 'compareAttribute' => 'start_date'],
            // [['start_date'], 'compare', 'operator' => '>=', 'compareValue' => date('Y-m-d H:i:s')],
            [['name'], 'string', 'max' => 255],
            [['all_day'], 'string', 'max' => 1],
            [['userIDs'], 'safe'],
        ];
    }

    function behaviors(){
        return [
            'timestamp' => \yii\behaviors\TimestampBehavior::className(),
            'blameable' => \yii\behaviors\BlameableBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('nc', 'ID'),
            'user_id' => Yii::t('nc', 'Id Owner'),
            'name' => Yii::t('nc', 'Name'),
            'note' => Yii::t('nc', 'User Note'),
            'all_day' => Yii::t('nc', 'All Day'),
            'start_date' => Yii::t('nc', 'Start Date'),
            'end_date' => Yii::t('nc', 'End Date'),
            'userIDs' => Yii::t('nc', 'Attendants'),
        ];
    }

	function beforeDelete(){
		Invitation::deleteAll(['meeting_id' => $this->id]);
		return parent::beforeDelete();
	}


    function getOwner(){
      return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    function getUpdatedBy(){
      return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
    function getCreatedBy(){
      return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    function getInvitations(){
      return $this->hasMany(Invitation::className(), ['meeting_id' => 'id']);
    }

    function getUsers(){
      return $this->hasMany(User::className(), ['id' => 'user_id'])->via('invitations');
    }
    public function saveUser()
    {
        if (!is_array($this->userIDs)) {
            return;
        }
        if (!$this->isNewRecord) {
            Invitation::deleteAll(['meeting_id' => $this->id]);
        }
        foreach ($this->userIDs as $id) {
            $res = new Invitation();
            $res->meeting_id = $this->id;
            $res->user_id = $id;
            $res->save();
        }
    }
    public function loadUser()
    {
        $this->userIDs = ArrayHelper::getColumn($this->users, 'id');
    }

}
