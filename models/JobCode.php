<?php

namespace nc\timesheet\models;

use Yii;

/**
 * This is the model class for table "jobcode".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 */
class JobCode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%job_code}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['color'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('nc', 'ID'),
            'name' => Yii::t('nc', 'Name'),
            'description' => Yii::t('nc', 'Description'),
        ];
    }

    function behaviors(){
        return [
            'timestamp' => \yii\behaviors\TimestampBehavior::className(),
            'blameable' => \yii\behaviors\BlameableBehavior::className(),
        ];
    }
    function getUpdatedBy(){
      return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
    function getCreatedBy(){
      return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
}
