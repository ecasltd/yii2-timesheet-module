<?php

namespace nc\timesheet\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use nc\timesheet\models\TimeEntry;

/**
 * TimeEntrySearch represents the model behind the search form about `nc\timesheet\models\TimeEntry`.
 */
class TimeEntrySearch extends TimeEntry
{
    public $dateFrom, $dateTo;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'jobcode_id'], 'integer'],
            [['timesheet_id', 'date', 'description'], 'safe'],
            [['duration'], 'number'],
            [['dateFrom', 'dateTo'], 'date', 'format' => 'php:Y-m-d'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TimeEntry::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'jobcode_id' => $this->jobcode_id,
            'date' => $this->date,
            'duration' => $this->duration,
        ]);

        $query->andFilterWhere(['like', 'timesheet_id', $this->timesheet_id])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
