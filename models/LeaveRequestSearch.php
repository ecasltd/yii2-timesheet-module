<?php

namespace nc\timesheet\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use nc\timesheet\models\LeaveRequest;

/**
 * LeaveRequestSearch represents the model behind the search form about `nc\timesheet\models\LeaveRequest`.
 */
class LeaveRequestSearch extends LeaveRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'approved_by', 'state', 'number'], 'integer'],
            [['code', 'note', 'approval_note', 'all_day', 'start_date', 'end_date', 'approved_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LeaveRequest::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'approved_by' => $this->approved_by,
            'state' => $this->state,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'number' => $this->number,
            'approved_at' => $this->approved_at,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'approval_note', $this->approval_note])
            ->andFilterWhere(['like', 'all_day', $this->all_day]);

        return $dataProvider;
    }
}
